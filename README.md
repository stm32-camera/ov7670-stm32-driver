<br />
<p align="center">
  <a>
    <img src='./.assets/img/icon.png' width="20%" height="20%">
  </a>

  <h3 align="center">ov7670-stm32-driver</h3>
  <h3 align="center">A STM32 digital camera using the OV7670 and a 1.8inch LCD screen.</h3>
  <center><a href="https://gitlab.com/stm32-camera/ov7670-stm32-driver/-/issues">Issues</a></center>
  <center><a href="https://gitlab.com/stm32-camera/ov7670-stm32-driver/-/merge_requests">Pull Requests</a></center>
</p>

[![Documentation Status](https://readthedocs.org/projects/stm32-camera/badge/?version=latest)](https://stm32-camera.readthedocs.io/en/latest/?badge=latest)

# About

A simple driver for the OV7670 on the STM32 platform. This is the base of the project. Built behind the F4 MCU family, NUCLEO-F446ZE.

# Purpose

This is a project to test my skills with writing tests for embedded systems and creating drivers from data-sheets.

# Docs

Docs are hosted at https://stm32-camera.readthedocs.io/ they are rebuilt automatically every commit or PR.
Doxygen is used to create the docs.

# Setting up

## Getting the repo

To get the repo, you will need git installed on your computer.

Open a terminal and type `git clone https://gitlab.com/stm32-camera/ov7670-stm32-driver --recursive`,
this will get the repo and all the sub modules of the drivers along side it in one big clone.

## Prerequisites
This project requires, Ceedling for the tests.

Get Ceedling through the 'gem' ruby package manager. On Fedora Linux install this by running

`sudo dnf install gem`

Then install Ceedling by running

`gem install ceedling`

## Installing build tools
This project uses a lot of build tools to compile the project.

The main list of them is as follows:

`arm-none-eabi-gcc
 arm-none-eabi-gcc-cs
 cmake
 make
 meson
 arm-none-eabi-newlib
 openocd
 `

These can be installed through your distro's repos.

## Building the code

After you have all the build tools, go into the root of the project and type `./build.sh`.

This will then build and link the project into .bin and .elf files. It will then prompt you to press y or n for running the tests. This will be
run when a new PR is created or when code is pushed but it is good practise to always run these after making a few changes and before pushing.

## Installing STM32CubeIDE

If on Windows get STM32CubeIDE from the ST website (https://www.st.com/en/development-tools/stm32cubeide.html) and run and install the application per instructions.

If on a Linux based OS, the easiest way to install the STM32CubeIDE is through flathub, configuring flatpak and flathub on your distro is as easy as typing a few commands, see here https://flatpak.org/setup/

Once flathub is configured, into a new terminal, type:

`sudo flatpak install com.st.STM32CubeIDE`

to install the IDE regardless of distribution.

## Getting ST-Link tools

Talking to the development board is nessisary to flash and debug the code, on Windows the drivers can be installed by getting the single binary file from this Github page https://github.com/stlink-org/stlink .

On Linux, getting the tools can be done by install the `stlink-tools` in your distributions repos, see here for more help https://github.com/stlink-org/stlink .

# Configuration

Configuring the application is pretty simple, open the cloned repo in STM32CubeIDE by pressing on File->Import, then type 'ioc' and select the option.

From here select the .ioc file in the cloned directory, this will open the code in your IDE, you from here can run or debug the application and make edits to the .ioc file.

## Running in Atom or Vscode

Coming soon...

## Building doxygen docs manually
To build the doxygen documentation manually, run `doxygen doxygen.yml` in the root of the cloned directory. This will output the documentation in the `Documentation/` folder.

# Devices used

For this project the OV7670 camera, a 1.8inch adafruit ST7735R display, £15 logic analyser and a NUCLEO-F446ZE.

Links are here:

OV7670: https://www.amazon.co.uk/AZDelivery-Camera-Module-OV7670-Parent/dp/B0797QDDPJ

Display: https://www.adafruit.com/product/358

Logic analyser: https://www.amazon.co.uk/Hobby-Components-24MHz-Analyser-1-1-16/dp/B00DAYAREW

Board: https://www.mouser.co.uk/ProductDetail/STMicroelectronics/NUCLEO-F446ZE?qs=mKNKSX85ZJfEpCMkaFyEVw%3D%3D

Had to change boards because the last one was out of stock till August, the F446ZE will do.

# Images

Coming soon...

# Video

Coming soon...

# Helpful links

https://arduino.stackexchange.com/questions/24645/accessing-configuration-registers-of-ov7670-cam-on-i2c
