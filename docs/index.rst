.. stm32-camera documentation master file, created by
   sphinx-quickstart on Fri May 20 12:00:23 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to stm32-camera's documentation!
========================================

A simple driver for the OV7670 on the STM32 platform. This is the base of the project. Built behind the F4 MCU family, NUCLEO-F429ZI.

Installation
------------

Refer to the README in the root of the project directory https://gitlab.com/stm32-camera/ov7670-stm32-driver

Contribute
----------

- Issue Tracker: https://gitlab.com/groups/stm32-camera/-/issues
- Base source Code: https://gitlab.com/stm32-camera/ov7670-stm32-driver

Support
-------

If you are having issues, please let me know.
Create an issue on the base of the project of the whole group.

License
-------

The project is licensed under the MIT license. See the LICENSE in the root of the
main projects directory for more information.
